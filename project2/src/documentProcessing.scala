import com.github.aztek.porterstemmer.PorterStemmer
import scala.collection.mutable.HashMap

object cachedPorterStemmer{
  val lookup = new HashMap[String, String]()
  def getStemmedWord(word: String): String = {
    lookup.get(word) match{
      case Some(i) => return i
      case None => {
        val stemmed = PorterStemmer.stem(word)
        lookup(word) = stemmed
        return stemmed
      }
    }
  }
}