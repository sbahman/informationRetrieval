import scala.collection.mutable.ArrayBuffer 
import java.io.PrintWriter


class featureExtraction {
  val featureVector = new ArrayBuffer
  val outputFile = new PrintWriter("data/SVM-training.txt")
  
  def printFeatures(id:String, features:Array[Double]){
    outputFile.write(id)
    var index = 1
    features.foreach(x => outputFile.write(" " + index +":" + x))
    outputFile.write("\n")
  }
}