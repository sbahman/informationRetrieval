import scala.util.control.Breaks._

object HelloWorld {
    def main(args: Array[String]) {
      val topics = new topicReader(args(0))
      topics.read()
      
      //val features = new extractor(topics.list, "/tmp/tipster/zip") 
        
      //val process = new processor("/tmp/tipster/qrels")  
        
      if (args(3) == "-t"){        
        val termM = new termModel(topics.list, args(1))
        termM.output()
        val evaT = new evaluator("ranking-t-28.run", args(2), 100)
        evaT.printOverAll()
      }
     
      if (args(3) == "-l"){
        val langM = new langModel(topics.list, args(1))
        langM.output()
        val evaL = new evaluator("ranking-l-28.run", args(2), 100)
        evaL.printOverAll()
      }
      
      if (args(3) == "-r"){
        val regM = new extractor(topics.list, args(1))
        regM.output()
        val evaL = new evaluator("ranking-r-28.run", args(2), 100)
        evaL.printOverAll()
      }
    }
  }  

