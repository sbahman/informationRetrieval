import scala.Double
import ch.ethz.dal.tinyir.processing.TipsterCorpusIterator
import ch.ethz.dal.tinyir.processing.TipsterParse

class pair(docName: String, docScore: Double)
{
  val id = docName
  val score = docScore
}

class scoreFunc(querys: Array[topic], docIt: TipsterCorpusIterator)
{
  var scores = new Array[Array[pair]](querys.size)
  
  def process(){
    while(docIt.hasNext){
	    	val doc = docIt.next
	    	val tokens = doc.tokens
	    	
	    	var i = 0
	    	while(i < querys.size){
	    	  scores(i) = (scores(i) :+ new pair(doc.name, score(querys(i), doc)))
	    	  i += 1
	    	}
	  }
    
    
  }
  
  def score(query: topic, doc: TipsterParse) : Double = {
    0
  }
  
  def sort(){
    var i = 0
    while(i < scores.size){
      scores(i) = scores(i).sortWith(_.score > _.score)
      i += 1
    }
  }
  
  def result(){
    var i = 0
    while(i < querys.size){
      println(querys(i).id)
      println(querys(i).query)
      var j = 0
      while(j < 100){
        println(scores(i)(j).id)
        println(scores(i)(j).score)
        j += 1
      }
      i += 1
    }
  }
}