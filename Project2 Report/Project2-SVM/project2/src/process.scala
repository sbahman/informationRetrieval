import java.util.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.MutableList
import java.io.PrintWriter

/*
class evaluator(resultPath: String, truthPath: String, numDoc: Double)
{
  val resultFile = scala.io.Source.fromFile(resultPath).mkString.split("[\n]").toList.map { x => x.split("[ ]").toList }
  val truthFile = scala.io.Source.fromFile(truthPath).mkString.split("[\n]").toList.map { x => x.split("[ ]").toList }
  
  //resultFile.foreach(println)
  //truthFile.foreach(println)
  
  var truth = new ArrayBuffer[HashMap[String, Boolean]](0)
  var score = new ArrayBuffer[measure](0)
  
  // populate truth and score
  var num = 0
  var former = "0"
  for(piece: List[String] <- truthFile){
    // see if the current element is a new topic, if so, extend score and truth
    if(piece(0) != former){
      truth += new HashMap[String, Boolean]
      score += new measure
      num += 1
      former = piece(0)
    }
    val relate = if(piece(3) == "1") true else false
    if(relate) score(num-1).numPos += 1
    else score(num-1).numNeg += 1
    truth(num-1).put(piece(2).replaceAll("[-]", ""), relate)
  }
  
  
  val output = new PrintWriter("labeled-" + resultPath)
  num = 0
  former = "0"
  for(piece: List[String] <- resultFile){
    if(piece(0) != former){
      //score = (score :+ new measure)
      num += 1
      former = piece(0)
    }

    if(truth(num-1).containsKey(piece(2))) // is document in ground truth?
    {
      if(truth(num-1).get(piece(2))){      // true positive or false positive based on ground truth
        score(num-1).truePositive += 1
        score(num-1).calcAP                // since the document was relevant, update ap
        output.write("1")
      }
      else{
        score(num-1).falsePositive += 1
        score(num-1).kfPositive += 1
        output.write("0")
      }
    }
    else{
      score(num-1).falsePositive += 1
      score(num-1).unkPositive += 1     // if document is not in ground truth, it is unknown positive
      output.write("-1")
    }
    piece.foreach(x => {
      output.write(" " + x)
    })
    output.write("\n")
  }
  output.close
  
  score.foreach(x => x.calcAll)
  
  val overAll = new measure
  
  var numQuer = 0;
  score.foreach(x => {
    numQuer += 1
    overAll.numPos += x.numPos
    overAll.numNeg += x.numNeg
    overAll.truePositive += x.truePositive
    overAll.falsePositive += x.falsePositive
    overAll.kfPositive += x.kfPositive
    overAll.unkPositive += x.unkPositive
    overAll.ap += x.ap
  })
  
  overAll.calcSimpleMetrics
  overAll.ap = overAll.ap/numQuer
  if(overAll.truePositive != 0)
    overAll.fone = 2.0*overAll.precision*overAll.recall/(overAll.precision+overAll.recall)
  
  def printOverAll()
  {
    overAll.printMeasure()
  }
}
*/