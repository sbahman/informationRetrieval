import ch.ethz.dal.tinyir.io.DirStream
import java.io.PrintWriter
import java.io.File
import scala.io.Source
import java.io.BufferedReader
import java.io.FileReader
import java.io.IOException
import scala.math
import scala.collection.mutable.ArrayBuffer
import java.util.HashMap
import scala.util.Try
import scala.util.control.NonFatal
import java.util.Comparator
import scala.util.Sorting
//import scala.collection.mutable.PriorityQueue
import java.util.PriorityQueue
import sun.misc.Resource
import java.util.ArrayList
import util.control.Breaks._










class svmRel {
  def getEvaluationFile(svmEvaluationDir: String, evaltionFile: String, testingFilePath: String, numTopics: Int = 50){
    val n = 100
    val evaluation = new PrintWriter(evaltionFile)
    val best = new Array[PriorityQueue[svmDocument]](numTopics)
    
    val dir = new File(svmEvaluationDir);
    val directoryListing = dir.listFiles();
    if (directoryListing != null) {
      directoryListing.foreach(doc => {
        println(doc.getName)
        val topic = getTopicNum(doc)
        val k = topic - 50
        val br = new BufferedReader(new FileReader(doc))
        val brNames = new BufferedReader(new FileReader(testingFilePath + doc.getName))
        var line = new String
        var NameLine = new String
        var i = 0;
        while ({line = br.readLine(); line != null}) {
          NameLine = brNames.readLine()
          val stats = line.split(" ")
          val nameInfo = NameLine.split(" ")
          val current = new svmDocument(topic, nameInfo(1), stats(1).toFloat)
          best(k).add(current)
          i += 1
          if (i >= n)
            best(k).remove()
        }
      })
    }
    outputPriorityQueue(evaluation, best)
  }
  
  def outputPriorityQueue(output: PrintWriter, best: Array[PriorityQueue[svmDocument]]){
    val n = 100
    for(i <- 0 until best.length){
      var res = new ArrayList[svmDocument](0)
      while(!best(i).isEmpty()){
        val out = best(i).peek()
        res.add(out)
        best(i).remove()
      }
      var j = res.size-1
      while(j >= 0){
        output.write(res.get(j).topicNum + " ")
        output.write((n - j) + " ")
        output.write(res.get(j).name.replaceAll("-", "") + "\n")
        j -= 1
      }
    }
    output.close()
  }
      
  def getFullTraining(dirpath: String, truthFile: String, trainingFile: String = "data/training/processed/training.txt", testingFilePath: String = "data/test/"){
    val output = new PrintWriter(trainingFile)
    /*tried using dir stream
    val docs = new DirStream(dirpath, extension)
    */
    
    val lastTrainingTopic = 90 //everything above 90 is a test topic
    
    val dir = new File(dirpath);
    val directoryListing = dir.listFiles();
    if (directoryListing != null) {
      val br0 = new BufferedReader(new FileReader(directoryListing.head))
      val nfeatures = ((br0.readLine()).split(" ")).length -2              // get first line of first file, split by " " and subtract 2 to get number of features (first two indexes are not features)
      directoryListing.foreach(doc => {
        println(doc.getName)
        val testFile = new PrintWriter(testingFilePath + doc.getName)
        val k = getTopicNum(doc) - 50
        val isTest = (k > 40)
        val mean = getMean(doc, nfeatures)
        val std  = getStdDev(doc, nfeatures, mean)
        if (isTest)
          outputTest(doc, mean, std, nfeatures, testFile)
        else{
          val truth = getTruth(truthFile)
          outputTrain(doc, output, mean, std, nfeatures, k, truth, testFile, true)
        }
      })
    }
  }
  
  def getMean(doc: File, nfeatures: Int): Array[Float] = {
    val br = new BufferedReader(new FileReader(doc))
    val mean = new Array[Float](nfeatures)
    var line = new String
    var n = 0
    while ({line = br.readLine(); line != null}) {
      try{
        n +=1
        val features = line.split(" ")
        for(i <- 0 until nfeatures){
          mean(i) = mean(i) + features(i + 2).toFloat
        }
      }
      catch{ 
        case NonFatal(t) => println(line)
      }
    }
    for(i <- 0 until nfeatures){
      mean(i) = mean(i)/(n)
    }
    return mean
  }
  
  def getStdDev(doc: File, nfeatures: Int, mean: Array[Float]): Array[Float] = {
    val br = new BufferedReader(new FileReader(doc))
    val std = new Array[Float](nfeatures)
    var line = new String
    var n = 0
    while ( {line = br.readLine(); line != null} ) {   //uggly code should be done with sth like continually but got no time
      try{
        n +=1
        val features = line.split(" ")
        for(i <- 0 until nfeatures){
          std(i) = std(i) + ((features(i + 2).toFloat - mean(i))*(features(i + 2).toFloat - mean(i)))
        }      
      }
      catch{ 
        case NonFatal(t) => println(line)
      }
    }
    for(i <- 0 until nfeatures){
      std(i) = math.sqrt(std(i)/(n-1)).toFloat
    }
    return std
  }
  
  def getTruth(truthPath: String = ""): ArrayBuffer[HashMap[String, Boolean]] = {
    val truthFile = scala.io.Source.fromFile(truthPath).mkString.split("[\n]").toList.map { x => x.split("[ ]").toList }
    var truth = new ArrayBuffer[HashMap[String, Boolean]](0)
  
    // populate truth and score
    var num = 0
    var former = "0"
    for(piece: List[String] <- truthFile){
      // see if the current element is a new topic, if so, extend score and truth
      if(piece(0) != former){
        truth += new HashMap[String, Boolean]
        num += 1
        former = piece(0)
      }
      val relate = if(piece(3) == "1") true else false
      truth(num-1).put(piece(2).replaceAll("[-]", ""), relate)
    }
    return truth
  }
  
  
  //use this if topic is labeled
  def outputTrain(doc: File, outputFile: PrintWriter, mean: Array[Float],
      std: Array[Float], nfeatures: Int, kthDoc: Int, truth: ArrayBuffer[HashMap[String, Boolean]], testFilePrinter: PrintWriter, onlyLabelled: Boolean){
    val br = new BufferedReader(new FileReader(doc))
    var line = new String
    val scaledFeatures = new Array[Float](nfeatures)
    while ( {line = br.readLine(); line != null} ) {   //uggly code should be done with sth like continually but got no time
      try{
        val features = line.split(" ")
        for(i <- 0 until nfeatures){
          scaledFeatures(i) = (features(i + 2).toFloat - mean(i))/std(i)
        }
        var featureOutput = new String
        for(i <- 0 until nfeatures){
          if (scaledFeatures(i).isNaN())
            featureOutput += (" " + (i + 1).toString + ":" + 0.toString)
          else
            featureOutput += (" " + (i + 1).toString + ":" + scaledFeatures(i).toString)
        }
        
        var label = new String
        if( truth(kthDoc).containsKey(features((1)))) // is document in ground truth?
        {
          if(truth(kthDoc).get(features(1))){         //relevant or irelevant?
            label += "1"
          }
          else{
            label += "-1"
          }
          if(onlyLabelled){
            outputFile.write(label + featureOutput + "\n")
          }
        }
        else{
          label += "-1"
        }
        if(!onlyLabelled){
          outputFile.write(label + featureOutput + "\n")
        }
        testFilePrinter.write(label + featureOutput + "\n")
      }
      catch{
        case NonFatal(t) => {
          println(line)
          println(t.toString())
        }
      }
    }
  }
  
  //use this if topic is labeled
  def outputTest(doc: File, mean: Array[Float], std: Array[Float], nfeatures: Int, testFilePrinter: PrintWriter){
    val br = new BufferedReader(new FileReader(doc))
    var line = new String
    val scaledFeatures = new Array[Float](nfeatures)
    while ( {line = br.readLine(); line != null} ) {   //uggly code should be done with sth like continually but got no time
      try{
        val features = line.split(" ")
        for(i <- 0 until nfeatures){
          scaledFeatures(i) = (features(i + 2).toFloat - mean(i))/std(i)
        }
        var featureOutput = new String
        for(i <- 0 until nfeatures){
          if (scaledFeatures(i).isNaN())
            featureOutput += (" " + (i + 1).toString + ":" + 0.toString)
          else
            featureOutput += (" " + (i + 1).toString + ":" + scaledFeatures(i).toString)
        }
        val label = "0"
        testFilePrinter.write(label + featureOutput + "\n")
      }
      catch{
        case NonFatal(t) => {
          println(line)
          println(t.toString())
        }
      }
    }
  }

  
  def getTopicNum(doc: File): Int = {
    val name = doc.getName.toString
    val num = name.replaceFirst("[.][^.]+$", "");
    return num.toInt
  }
}

class svmDocument(id: Int, docName: String, probability: Float) extends Ordered[svmDocument]
{
  val topicNum = id.toString()
  val name = docName
  val prob = probability 
  
  def compare(that: svmDocument) = {
    val dif = this.prob - that.prob
    if(dif > 0) -1
    else if(dif < 0) 1
    else 0
  }
}

class svmComparator extends Comparator[svmDocument]{
  def compare(x: svmDocument, y:svmDocument) = {
    val dif = x.prob - y.prob
    if(dif > 0) 1
    else if(dif < 0) -1
    else 0
  }
}





