import scala.util.control.Breaks._

object HelloWorld {
    def main(args: Array[String]) {
      val dataFolder = "/tmp/"
      //val dataFolder = "data/"
      
      val getFeature = false
      val getResults = false
      val evaluateTerm = false
      val evaluateLang = false
      val getSVMTraining = true
      
      val topics = new topicReader(dataFolder + "tipster-2/testTopics")
        topics.read()
      
      if(getFeature){
        val features = new extractor(topics.list, dataFolder + "tipster-2/zip", dataFolder + "training/unprocessed")
      }
      
      if (getResults){        
        val termM = new termModel(topics.list, dataFolder + "tipster/zip")
        //termM.output()
        
        val langM = new langModel(topics.list, dataFolder + "tipster/zip")
        //langM.output()
      }
      
      if (evaluateTerm){
        val evaT = new evaluator("ranking-t-28.run", dataFolder + "tipster/testTopics", 100)
        evaT.printOverAll()
      }
      
      if (evaluateLang){
        val evaL = new evaluator("ranking-l-28.run", dataFolder + "tipster/testTopics", 100)
        evaL.printOverAll()
      }
      
      if (getSVMTraining){
        val svm = new svmRel
        svm.getFullTraining(dataFolder + "training/unprocessed",  dataFolder + "tipster-2/qrels", dataFolder + "training/processed/training.txt", dataFolder + "test/input/")
      }
      println("done")
      
    }
  }  

