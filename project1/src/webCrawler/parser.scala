package webCrawler

import java.net.URL
import java.util.HashSet
import java.net.HttpURLConnection
import java.io.ByteArrayOutputStream

import scala.collection.mutable.Queue
import scala.io.Source.fromInputStream

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import webCrawler.Detector
import webCrawler.Counter

class Crawler(source: String) 
{
  private var getCounter = 0
  private var linkCounter = 0
  
  private val queue = new Queue[String]
  private val linkRegex = "href=\"(.+?)\"".r
  private val crawledPool = new HashSet[String]
  private val host = new URL(source).getHost.toString
  
  private val counter = new Counter
  private val detector = new Detector
  
  //private val writer = new PrintWriter(new File("Links.txt"))

  // public method to start crawling
  def crawl {
    queue += source
    crawledPool.add(source)
    
    while(queue.size != 0){
      crawlProcess(queue.dequeue())
    }
  }

  // the crawling pipeline
  private def crawlProcess(url: String) {
    if(getCounter%100==0 && getCounter!=0) {
      println("Crawling: " + getCounter + " / " + linkCounter)
    } 
      
    try{
      val content = getContent(url)
      
      val doc = Jsoup.parse(content)
      val text = doc.body.text
      detector.processContent(url, text)
      counter.count(url.toLowerCase, content.toLowerCase)
      
      val links = for ( link <- linkRegex.findAllIn(content).matchData ) 
         yield { link.group(1) }
      
      val list = links.filter{ link =>
        (link.endsWith(".html") || link.endsWith("/")) &&    // take htmls
        (!link.startsWith("http") || link.contains(host)) && // remain in server
        !link.contains("?") && !link.contains("#")           // disregard ? and #
      }.map{
        link => link match{
          case link if link.startsWith("http") => link
          case link if link.startsWith(".") => getLink(url, link)
          case link if link.startsWith("/") => url.substring(0, url.lastIndexOf("/")) + link
          case _ => url.substring(0, url.lastIndexOf("/")) + "/" + link
        }
      }
      
      list.foreach( link => {
        if(!crawledPool.contains(link)) {
          queue += link
          crawledPool.add(link)
          linkCounter += 1
        }
      })
    }
    catch{
      case e: Exception => { 
        //println(e)
        //e.printStackTrace
      }
    }
  }
  
  // get content from a page
  private def getContent(link: String) = {
    getCounter = getCounter + 1
    val url = new URL(link);
    val con = url.openConnection();
    con.setConnectTimeout(100)
    con.setReadTimeout(100)
    val content = fromInputStream( con.getInputStream, "UTF-8").mkString
    content
  }
  
  // deal with relative paths
  private def getLink(url: String, path: String) = {
    var index1 = url.lastIndexOf("/")
    var index2 = path.indexOf("/")
    while(index2 != -1 && path(index2-1) == '.'){
      index1 = url.lastIndexOf("/", index1-1)
      index2 = path.indexOf("/", index2+1)
    }
    url.substring(0, index1) + path.substring(path.lastIndexOf("../")+2, path.size)
  }
  
  // print results
  def result {
    println("Distinct URLs found: " + linkCounter)
    counter.result
    detector.result
  }
}
