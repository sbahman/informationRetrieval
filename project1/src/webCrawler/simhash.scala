package webCrawler

//adapted from https://github.com/mmikulicic/pace/blob/master/src/main/scala/afm/distance/simhash.scala

import scala.collection.mutable._
import scala.math.min

trait Simhash {
  val bits = 128

  def features(tokens: List[String]) = tokens.sliding(3).map(_.hashCode)

  def simhash(tokens: List[String]): Int

  def rotatedSimhash(tokens: List[String], step: Int) = {
    val hash = simhash(tokens)
    for (i <- new Range(0, bits, step))
      yield Integer.toHexString(Simhash.rotated(hash, i)).reverse.padTo(bits / 4, "0").reverse.mkString
  }

}

class AdditiveSimhash extends Simhash {
  def simhash(tokens: List[String]) = features(tokens).reduceLeft(_ | _)
}

class MaxSimhash(val repeat: Int) extends Simhash {
  def simhash(tokens: List[String]) = {
    val q = new PriorityQueue[Int]

    for (feature <- tokens.sliding(3).map(_.hashCode))
      q += feature

    var sim = 0
    if (q.length > 0)
      for (i <- 0 until min(repeat, q.length))
        sim ^= q.dequeue
    sim
  }
}

class BalancedSimhash extends Simhash {
  def simhash(tokens: List[String]) = {
    val v = new Array[Int](bits)

    for (feature <- features(tokens)) {
      var n = feature
      for (b <- 0 until bits) {
        v(b) += (if ((n & 1) == 1) 1 else -1)
        n = n >>> 1
      }
    }

    var sim = 0
    for (b <- v.map(_ > 0))
      sim = sim << 1 | (if (b) 1 else 0)
    sim
  }
}

object Simhash {
  val bits = 32

  def simhash = new MaxSimhash(100).simhash(_)

  def rotated(i: Int): Int = rotated(i, 1)
  def rotated(i: Int, bits: Int): Int = Integer.rotateLeft(i, bits)
}