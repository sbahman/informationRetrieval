package webCrawler

import webCrawler.Crawler
import java.util.Date
import java.text.SimpleDateFormat

// Note it nows uses command line argument
// add http://idvm-infk-hofmann03.inf.ethz.ch/eth/www.ethz.ch/en.html
// to your run config. - arguments - program arguments

object Main {
  def main(args: Array[String]) = 
  {
    val crawler = new Crawler(args(0))
    crawler.crawl
    crawler.result
    val sdf= new SimpleDateFormat("yyyy-mm-dd hh:mm:ss")
    println(sdf.format(new Date()))
  }
  // 7490 links found
}